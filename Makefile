SRC_DIR := src
BUILD_DIR := build
DATA_DIR := data
CXXFLAGS := -o $(BUILD_DIR)/alinha -std=c++20 `pkg-config --cflags --libs opencv4`

default: main get_data

debug: main_debug get_data

main: $(SRC_DIR)/alinha.cpp
	g++ -O3 $(CXXFLAGS) $(SRC_DIR)/alinha.cpp
main_debug: $(SRC_DIR)/alinha.cpp
	g++ -O0 -ggdb3 $(CXXFLAGS) $(SRC_DIR)/alinha.cpp 
get_data: $(DATA_DIR)/rice_1to1000/readme.txt
	cd data; ./getData.sh
clean:
	rm -rf build/*
	rm -rf out/*


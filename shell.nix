{ pkgs ? import <nixpkgs> {} }:
let
  python-with-my-packages = pkgs.python3.withPackages (p: with p; [
    matplotlib
    numpy
    scipy
    opencv4
    tensorflow
    pydot
  ]);
in
pkgs.mkShell {
  buildInputs = [
    python-with-my-packages
    # other dependencies
    pkgs.gcc
    pkgs.gdb
    pkgs.gnumake
    # pkgs.eigen
    pkgs.opencv4
    pkgs.pkg-config
    pkgs.unzip
    pkgs.graphviz
  ];
  
  shellHook = ''
    PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}
    # maybe set more env-vars
  '';
}

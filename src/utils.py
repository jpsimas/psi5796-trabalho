import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

import tensorflow.keras as keras
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dropout, Conv2D, MaxPooling2D, Dense, Flatten, AveragePooling2D, BatchNormalization, GlobalAveragePooling2D, Softmax, BatchNormalization, Activation
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras import optimizers, Input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

def impHistoria(history, name = ""):
  print(history.history.keys())
  plt.figure()
  plt.plot(history.history["accuracy"])
  plt.plot(history.history["val_accuracy"])
  plt.title("Model Accuracy"); plt.ylabel("Accuracy"); plt.xlabel("Epoch")
  plt.legend(["train", "test"], loc="upper left")
  #plt.show()
  plt.savefig("../out/" + name + "_acc.eps")
  plt.figure()
  plt.plot(history.history["loss"])
  plt.plot(history.history["val_loss"])
  plt.title("Model Loss"); plt.ylabel("Loss"); plt.xlabel("Epoch")
  plt.legend(["Train", "Test"], loc="upper left")
# plt.show()
  plt.savefig("../out/" + name + "_loss.eps")

def readData(path, extension, image_size, categories, subDirs = True):
    data = np.ndarray((5000, image_size, image_size, 3))
    labels = np.ndarray((5000), dtype = int)
    
    for i in range(len(categories)):
        if(subDirs):
            pathI = path + categories[i] + "/"
        else:
            pathI = path
        for j in range(1000):
            k = 1000*i + j
            labels[k] = i
            
            fName = categories[i] + " (" + str(j + 1) + ")"
            
            # print("Reading " + pathI + fName + ".jpg")
            img = cv.imread(pathI + fName + "." + extension)
    
            # subamostra imagens para image_size x image_size
            if(image_size != 250):
              img = cv.resize(img, (image_size, image_size), interpolation=cv.INTER_LANCZOS4)
            
            data[k, :, :, :] = img
            
    print(data.shape)
    print(labels.shape)
    print(labels[0:10])
    
    # normaliza dados
    data /= 255
    data -= 0.5

    return (data, labels)

def splitData(n_a, data, labels, num_classes):    
    train_inds = np.ndarray((0), dtype = int)
    for i in range(num_classes):
        train_inds = np.concatenate((train_inds, np.arange(1000*i, 1000*i + n_a)))
    
    data_test = np.delete(data, train_inds, axis = 0)
    labels_test = np.delete(labels, train_inds)
    
    data = data[train_inds, :, :, :]
    labels = labels[train_inds]
    return (data, data_test, labels, labels_test)
        

def runSim(name, data, data_test, labels, labels_test, num_classes, batch_size, epochs):
    input_shape = data[0, :, :].shape
    
    labels = keras.utils.to_categorical(labels, num_classes)
    labels_test = keras.utils.to_categorical(labels_test, num_classes)
    
    print(data.shape)
    print(data_test.shape)
    
    print(labels.shape)
    print(labels_test.shape)
    
    model = Sequential([
      Conv2D(filters=64, kernel_size=(3, 3), padding = 'same', input_shape=input_shape),
      BatchNormalization(),
      Activation("relu"),
      MaxPooling2D(pool_size=(2, 2), strides=2),
      Conv2D(filters=128, kernel_size=(3, 3), padding = 'same'),
      BatchNormalization(),
      Activation("relu"),
      MaxPooling2D(pool_size=(2, 2), strides=2),
      Conv2D(filters=num_classes, kernel_size=(3, 3), padding = 'same'),
      BatchNormalization(),
      Activation("relu"),
      MaxPooling2D(pool_size=(2, 2), strides=2),
      GlobalAveragePooling2D(),
      Softmax()
       ])
    
    
    model.summary()
    # keras.utils.plot_model(model, to_file = "../img/model.svg", show_shapes=True, show_layer_names=False)
    
    opt = optimizers.Adam(learning_rate = 1e-2)
    model.compile(optimizer = opt, loss = "categorical_crossentropy", metrics = ["accuracy"])
    
    reduce_lr = ReduceLROnPlateau(monitor="loss", factor = 0.1,
                                  patience = 5, min_lr = 1e-5,
                                  verbose = 1)
    
    datagen = ImageDataGenerator(rotation_range=0,
                                 zoom_range = 0.05,
                                 vertical_flip=True,
                                 horizontal_flip=True) 
    
    history = model.fit(datagen.flow(data, labels, batch_size=batch_size),
                        steps_per_epoch=data.shape[0]//batch_size,
                        epochs = epochs, verbose = 2, validation_data = (data_test, labels_test),
                        callbacks = [reduce_lr])
    impHistoria(history, name)
    
    score = model.evaluate(data_test, labels_test, verbose=2)
    print("Test loss:", score[0])
    print("Test accuracy:", score[1])
    model.save("../out/cnn_" + name + ".h5")
    
    

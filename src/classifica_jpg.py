from utils import *
import sys

if(len(sys.argv) < 2):
  n_a = 100
  print("Nenhum argumento fornecido. Usando n_a = {}".format(n_a))
else:
  n_a = int(sys.argv[1])
  print("Usando n_a = {}".format(n_a))

if(len(sys.argv) < 3):
  epochs = 100
  print("Nenhum valor de epochs fornecido. Usando epochs = {}".format(epochs))
else:
  epochs = int(sys.argv[2])
  print("Usando epochs = {}".format(epochs))
  
image_size = 32

# le dataset
path = "../data/rice_1to1000/";
categories = ["Arborio", "Basmati", "Ipsala", "Jasmine", "Karacadag"];
data, labels = readData(path, "jpg", image_size, categories)

# plt.matshow(data[0, :, :, 1])
# plt.figure()
# plt.plot(labels)
# plt.show()

# divide conjuntos de teste e de treino
# n_a = 100#50 -> 96%, 100 -> 98.5%
num_classes = len(categories);
data, data_test, labels, labels_test = splitData(n_a, data, labels, num_classes)

print(data.shape)
print(data_test.shape)

print(labels.shape)
print(labels_test.shape)

batch_size = 32
#epochs = 100

runSim("jpg_" + str(n_a) + "_" + str(epochs), data, data_test, labels, labels_test, num_classes, batch_size, epochs)


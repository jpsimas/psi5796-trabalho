// PSI5796 - EP (alinha.cpp)
// Copyright (C) 2022 João Pedro de Omena Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>
#include <cmath>

template<typename T>
auto
getTransformMatrix(const cv::Mat_<T>& src) {
  auto m = moments(src, true);
  double cen_x = m.m10/m.m00;
  double cen_y = m.m01/m.m00;
  double theta = 0.5*atan2(2.0*m.mu11, m.mu20-m.mu02);
  
  double cosT = cos(theta);
  double sinT = sin(theta);

  cv::Mat_<double> rot(3, 3, 0.0);
  cv::Mat_<double> trans1(3, 3, 0.0);
  cv::Mat_<double> trans2(3, 3, 0.0);
  cv::Mat_<double> affine(2, 3, 0.0);

  trans1 <<
    1, 0, -cen_x,
    0, 1, -cen_y,
    0, 0, 1;

  rot <<
    cosT, sinT, 0,
    -sinT, cosT, 0,
    0, 0, 1;

  trans2 <<
    1, 0, src.rows/2,
    0, 1, src.cols/2,
    0, 0, 1;
  
  affine = cv::Mat_<double>::eye(2, 3)*trans2*rot*trans1;
  
  return affine;
  
}

void processFile(std::string fName, std::string path) {
  cv::Mat_<uint8_t> img;
  cv::Mat_<uint8_t> imgBin;
  
  //le imagem
  img = cv::imread(path + fName + ".jpg", cv::IMREAD_GRAYSCALE);

  //discretiza imagem
  cv::threshold(img, imgBin, 127, 255, cv::THRESH_BINARY);
      
  auto T = getTransformMatrix(imgBin);
  cv::warpAffine(img, img, T, img.size());
  cv::imwrite("../out/" + fName + ".png", img);
}

int
main() {

  std::cout << "PSI5796 - EP (alinha.cpp)  Copyright (C) 2022 João Pedro de Omena Simas"  << std::endl <<
    "This program comes with ABSOLUTELY NO WARRANTY." << std::endl <<
    "This is free software, and you are welcome to redistribute it" << std::endl <<
    "under certain conditions." << std::endl << std::endl;
  

  //processa imagens de teste
  std::string path = "../data/rice_1to1000/orientacao/";
  const std::string outPath = "../out/";
  std::array<std::string, 6> fNames = {"Arborio1", "Basmati1", "Ipsala1", "Jasmine1", "Jasmine2", "Karacadag1"};
  
  for(auto i = 0; i < fNames.size(); i++) {
    std::cout << "Processing " << fNames[i] << std::endl;
    processFile(fNames[i], path);
  }
  
  //processa imagens do dataset
  path = "../data/rice_1to1000/";
  std::array<std::string, 5> categories = {"Arborio", "Basmati", "Ipsala", "Jasmine", "Karacadag"};
  
  for(auto i = 0; i < categories.size(); i++) {
    std::string pathI = path + categories[i] + "/";
    for(auto j = 1; j <= 1000; j++){
      std::string fName = categories[i] + " (" + std::to_string(j) + ")";
      std::cout << "Processing " << fName << std::endl;
      processFile(fName, pathI);
    }
  }

  return 0;
}
